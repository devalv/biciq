import firebase from 'firebase';
export default firebase.initializeApp({
    apiKey: "AIzaSyASminrsP0A2OZznx6_D8Y4IapxQyPiFXE",
    authDomain: "bici-q.firebaseapp.com",
    databaseURL: "https://bici-q.firebaseio.com",
    projectId: "bici-q",
    storageBucket: "bici-q.appspot.com",
    messagingSenderId: "828610279423"
});
import { Permissions, Notifications,Google } from 'expo';
import {  AsyncStorage,Alert } from 'react-native';
export let db = firebase.database();
export const storage = firebase.storage();
export let auth = firebase.auth();
import axios from 'axios';
import {url} from './Config';
export async function getItem(item) {
    try {
      const value = await AsyncStorage.getItem(item);
      return value;
    } catch (error) {
        console.log('error getting item: ', error);
        return null;
    }
    return null;
}
export async function setItem(name,item) {
    try {
      const value = await AsyncStorage.setItem(name,item);
      return value;
    } catch (error) {
        console.log('error setting item: ', error);
        return null;
    }
    return null;
}
export async function removeItem(item) {
    try {
      const value = await AsyncStorage.removeItem(item);
      return value;
    } catch (error) {
        console.log('error removing item: ', error);
        return null;
    }
    return null;
}
export async function logOutUser(){
    try{
        auth.signOut();
        let uid = await AsyncStorage.getItem('uid');
        if(uid){
            db.ref(uid).remove();
        }
        const value = await AsyncStorage.clear()
        return value;
    }
    catch (error){
        console.log('something went wrong')
        return 'error';
    }
}
export function logInUser(email, password) {
    return auth.signInWithEmailAndPassword(email, password);
}
export async function verifyUser (uid){
    let user = null;
    await getBicis().then(async value=>{
        await Object.values(value.data).map(element=>{
            if(uid===element.token){
                console.log('element: ', element);
                user = element;
            }
        })
    });
    return user;
}
export function registerNewUser(email,password){
    return auth.createUserWithEmailAndPassword(email,password);
}
export function postGuia(user){
    let options ={
        async: true,
        method: 'POST',
        url:url+'Bicis',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
        data:user
    };
    return axios(options);
}

export function getBicis(){
    let options ={
        async: true,
        method: 'GET',
        url:url+'Bicis',
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
    };
    return axios(options);
}
export function updateUserLocation(user){
    let options ={
        async: true,
        method: 'PUT',
        url:url+'Bicis/'+user.id,
        data:user,
        headers: {
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache',
        },
    };
    return axios(options); 
}
export async function updateLocation(uid,location){
    let id = null;
    let data = null;
    await getBicis().then(async value=>{
        await Object.values(value.data).map(element=>{
            if(uid===element.token){
                console.log('element: ', element);
                id=element.id;
                data = element;
                data.location = location;
            }
        })
    });
    if(id!==null && data!==null){
        let options ={
            async: true,
            method: 'PUT',
            url:url+'Bicis/'+id,
            data:data,
            headers: {
              'Content-Type': 'application/json',
              'Cache-Control': 'no-cache',
            },
        };
        return axios(options);
    }
    else{
        console.log('error, no uid');
    }
}