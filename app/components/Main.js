import React,{Component} from 'react';
import { AppRegistry,Modal, View, Image,TouchableHighlight, AsyncStorage,FlatList ,Alert, WebView, Linking,ScrollView, Platform } from 'react-native';
import { Container, Label,Header, Radio,Toast, Input,Item,Form,Title, Content, Footer, FooterTab, Button, Left, Right, Picker,Body, Icon, Text, Spinner } from 'native-base';
import Swiper from 'react-native-swiper';
import {ListItem as ListItemN, List as ListN} from 'native-base';
import {List,ListItem,SearchBar} from 'react-native-elements';
import { Actions, ActionConst } from 'react-native-router-flux';
import comida from '../../assets/comida.png'; 
import cultura from '../../assets/cultura.png'; 
import diversion from '../../assets/diversion.png'; 
import mini from '../../assets/mini.png';
import filter from '../../assets/filter.png';
import price from '../../assets/price.png';
import google from '../../assets/google.png';
import facebook from '../../assets/facebook.png';
import main from '../../assets/main.png';
import { Constants, Location, Permissions,MapView } from 'expo';
import geofire from 'geofire';
import {getBicis,logOutUser,db,getFBGuides} from '../utils/Controller';
const locations = db.ref('locations');

var style = require('../utils/Styles.js');
const GOOGLE_MAPS_APIKEY='AIzaSyBaAJwtqoIkT0h3Mw41tXeVlWYwTL_lihs';
export default class Index extends Component{
  constructor(props) {
    super(props);
    this.state = { 
        error: '', 
        isLoading: false,
        bicis:null,
        category:'Todos',
        categories:[
          'Todos',
          'Cultura',
          'Comida',
          'Diversión'
        ],
        route:[],
        location:null,
        locations:null,
        footerTab:1,
    };
  }
  getData(){
    let self = this;
    getBicis().then(value=>{
      self.setState({bicis:value.data,isLoading:false})
    }).catch(error=>{
      console.log('error: ', error);
    })
  }
  async startOver(){
    this.setState({isLoading:true,loading:true,refreshing:true,});
    await this._getLocationAsync();
    this.getData();
  }

  renderListItemGuides = (val) =>{
    let item = val.item;
    return(
      <ListItemN onPress={()=> Actions.info({store:item}) }>
        <Left style={style.mediumFlex}>
          <Image style={style.miniIconList} source={{uri:item.photo}} resizeMode="contain"></Image>
        </Left>
        <Body>
          <Text>{item.nombre}</Text>
          <Text style={{fontSize:10,color:'#434343'}}>{'Edad: '+item.edad + ', Tipo de Guía: ' + item.estilo}</Text>
        </Body>
        <Right>
          <Button transparent onPress={()=> Actions.info({store:item}) }>
            <Icon name='ios-arrow-forward' style={{color:'#f26a45'}}/>
          </Button>
        </Right>
      </ListItemN> 
    );
  }
  _getLocationAsync = async () => {
    let { status } = await Permissions.askAsync(Permissions.LOCATION);
    if (status !== 'granted') {
      this.setState({
        error: 'Permission to access location was denied',
      });
    }
    let location = await Location.getCurrentPositionAsync({});
    let currentLocation = [location.coords.latitude, location.coords.longitude];
    this.setState({ location:currentLocation});
  };

  
  componentWillMount(){
    this.startOver();
  }
  async componentDidMount(){
    locations.on('value', snapshot => {
    this.setState({
        locations: snapshot.toJSON()
    })
    });
  }
render(){
  var markers = this.state.locations!==null &&
    Object.entries(this.state.locations).map((element, index)=>{
        let key = element[0];
        let bicis  = this.state.bicis;
        console.log('bicis: ', bicis);
        if(bicis!==null){
        for(let i = 0; i <bicis.length;i++){
          if(key===bicis[i].token){
            let coordenadas = [parseFloat(element[1].l[0]),parseFloat(element[1].l[1])];
            console.log('coordenadas: ', coordenadas);
            return(
                <MapView.Marker
                  key = {index}
                  coordinate={{latitude: coordenadas[0],longitude: coordenadas[1]}}
                  title={bicis[i].name}
                >
                <MapView.Callout containerStyle={style.containerCallout} onPress={()=>Actions.info({store:bicis[i]})}>
                    <View style={{width:240}}>
                    <View style={style.mainImageContainer}>
                        <Image
                            resizeMode="contain"
                            style={style.mainImageFloating}
                            source={{uri:bicis[i].photo}}
                        />
                    </View>
                    <Text style={{fontSize:14,marginVertical:5, textAlign:'center',}}>{bicis[i].name}</Text>
                    <Text style={{fontSize:11,marginBottom:5, textAlign:'center',}}>{bicis[i].info}</Text>
                    <Button style={{alignSelf:'center'}} onPress={()=>Actions.info({store:bicis[i]})} >
                        <Text style={{fontSize:14}}>Detalle</Text>
                    </Button>
                    </View>
                </MapView.Callout>
                </MapView.Marker>
            );
          }
        }
      }
      else{
        return '';
      }
    });
    if(this.state.isLoading == true){
      return(
          <View style={style.centerSpinner}>
              <Spinner />
          </View>
      )
    }
    return (
      <Container>
        <Header>
          <Body style={{flexWrap: 'wrap', alignItems: 'flex-start',flexDirection:'row',}}>
              <Title>Bici Q</Title>
          </Body>
        </Header>
          <Content>
          {
            this.state.location!==null?
              <Content>
                <MapView
                  style={style.mapFinder}
                  showsUserLocation={true}
                  zoomEnabled = {true}
                  showsMyLocationButton={true}
                  scrollEnabled = {true}
                  apikey={GOOGLE_MAPS_APIKEY}
                  initialRegion={{
                    latitude: parseFloat(this.state.location[0]),
                    longitude: parseFloat(this.state.location[1]),
                    latitudeDelta:  0.02922,
                    longitudeDelta:  0.02421,
                  }}
                >
                {markers}
                </MapView>
            </Content>
            :
            <Content>
                <Text>Por favor activa los servicios de localización</Text>
            </Content>
          }
          </Content>
      </Container>
    );
  }
}
